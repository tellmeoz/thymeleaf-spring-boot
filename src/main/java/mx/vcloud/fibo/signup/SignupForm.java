package mx.vcloud.fibo.signup;


import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import mx.vcloud.fibo.account.Account;


public class SignupForm {


    @NotBlank
	@Email
	private String email;

    @NotBlank
	private String password;

    public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Account createAccount() {
        return new Account(getEmail(), getPassword(), "ROLE_USER");
	}
}
